import React from 'react';
import styled from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';

import Stars from './Stars';

const Area = styled.TouchableOpacity`
    margin-bottom: 10px;
    border-radius: 10px;
    padding: 5px;
    flex-direction: row;
    width: 100%;
    height: 42%;
`;

const AreaAvatar = styled.TouchableOpacity`
    margin-bottom: 10px;
    border-radius: 10px;
    padding: 20px;
    flex: 1;
    width: 40%;
    justify-content: center;
    align-items: center;
`;

const AreaInfo = styled.TouchableOpacity`
    background-color: rgba(0, 0, 0, 0.2);
    border: 1px solid #1E3148;
    margin-bottom: 10px;
    border-radius: 15px;
    padding: 20px;
    flex-direction: row;
    width: 60%;
    margin-left:5px;
    
`;


const Avatar = styled.Image`
    width: 100px;
    height: 100px;
    border-radius: 10px;
    justify-content: center;
    align-items: center;
    padding:5px;

`;

const InfoArea = styled.View`
    justify-content: space-evenly;
    width: 100%;
    padding:5px;
`;

const UserName = styled.Text`
    font-size: 15px;
    font-weight: bold;
    color: #FFF;

`;

const SeeProfileButton = styled.View`
    width: 60px;
    height: 30px;
    border: 1px solid #fff;
    border-radius: 10px;
    justify-content: center;
    align-items: center;
    background-color: #1E3148;
`;

const SeeProfileButtonText = styled.Text`
    font-size: 15px;
    color: #FFFFFF;
    font-weight: bold;
`;


export default ({list}) => {
    const navigation = useNavigation();

    const handleClick = () => {
        navigation.navigate('Barber', {
            id: list.id,
            avatar: list.avatar,
            name: list.name,
            stars: list.stars
        });
    }

    return (
        <Area onPress={handleClick}>
            <AreaAvatar onPress={handleClick}>
                <Avatar source={{uri: list.avatar}} />
                <Stars stars={list.stars} showNumber={true} />
            </AreaAvatar>

            <AreaInfo onPress={handleClick}>
                <InfoArea>
                    <UserName>{list.name}</UserName>

                    <SeeProfileButton>
                        <SeeProfileButtonText>Perfil</SeeProfileButtonText>
                    </SeeProfileButton>
                </InfoArea>
            </AreaInfo>
        </Area>
        
       
    );
}