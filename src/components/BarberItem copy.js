import React from 'react';
import styled from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';

import Stars from '../components/Stars';

const Area = styled.TouchableOpacity`
    background-color: rgba(0, 0, 0, 0.4);
    border: 1px solid #1E3148;
    margin-bottom: 10px;
    border-radius: 10px;
    padding: 30px;
    flex-direction: row;
    width: 100%;
`;

const Avatar = styled.Image`
    width: 100px;
    height: 100px;
    border-radius: 10px;
`;

const InfoArea = styled.View`
    margin-left: 10px;
    justify-content: space-between;
`;

const UserName = styled.Text`
    font-size: 15px;
    font-weight: bold;
    color: #FFF;

`;

const SeeProfileButton = styled.View`
    width: 80px;
    height: 35px;
    border: 1px solid #fff;
    border-radius: 10px;
    justify-content: center;
    align-items: center;
    background-color: #1E3148;
`;

const SeeProfileButtonText = styled.Text`
    font-size: 15px;
    color: #FFFFFF;
    font-weight: bold;
`;


export default ({data}) => {
    const navigation = useNavigation();

    const handleClick = () => {
        navigation.navigate('Barber', {
            id: data.id,
            avatar: data.avatar,
            name: data.name,
            stars: data.stars
        });
    }

    return (
        <Area onPress={handleClick}>
            <Avatar source={{uri: data.avatar}} />
            <InfoArea>
                <UserName>{data.name}</UserName>

                <Stars stars={data.stars} showNumber={true} />

                <SeeProfileButton>
                    <SeeProfileButtonText>Perfil</SeeProfileButtonText>
                </SeeProfileButton>
            </InfoArea>
        </Area>
    );
}