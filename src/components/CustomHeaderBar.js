import React, { useContext } from 'react';
import styled from 'styled-components/native';
import Lottie from 'lottie-react-native';

import HomeIcon from '../assets/logoHeader.svg';
import LoadPerfil from '../animation/loadPerfil.json';
import IconHeader1 from '../animation/iconHeader1.json';


const TabHeader = styled.View`
    height: 70px;
    background-color: rgba(0, 0, 0, 0.2);
    justify-content: center;
    flex-direction: row;
    border: 1px solid #1E3148;
    justify-content: center;
    align-items: center;
    
`;

const TabItem = styled.TouchableOpacity`
    width: 50px;
    height: 40px;
    flex-direction: row;
    flex: 1;
    justify-content: center;
    align-items: center;
    margin-top: 5px;
    margin-bottom: 0px;
    margin-top: 6px;

`;

const TabItemCenter = styled.TouchableOpacity`
    width: 65px;
    height: 40px;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    border-radius: 5px;
    margin-top: 10px;
`;


export default () => {


    return (
        <TabHeader>
            <TabItemCenter >
                    <Lottie  style={{width:30, height:30}} resizeMode="contain" source={LoadPerfil} autoPlay loop />
                    <Lottie  style={{width:75, height:75}} resizeMode="contain" source={IconHeader1} autoPlay loop />
                    <Lottie  style={{width:30, height:30}} resizeMode="contain" source={LoadPerfil} autoPlay loop />
            </TabItemCenter>
        </TabHeader>
    );
}