import React from 'react';
import styled from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';

import Stars from '../components/Stars';

const Area = styled.TouchableOpacity`
    margin-bottom: 5px;
    border-radius: 10px;
    padding: 5px;
    flex-direction: row;
`;

const AreaAvatar = styled.TouchableOpacity`
    margin-bottom: 10px;
    border-radius: 10px;
    padding: 5px;
    width: 35%;
    justify-content: center;
    align-items: center;
    margin-bottom: 5px;
`;

const AreaInfo = styled.TouchableOpacity`
    border: 1px solid #1E3148;
    background-color: rgba(0, 0, 0, 0.4);
    margin-bottom: 5px;
    border-radius: 15px;
    padding: 5px;
    flex-direction: row;
    width: 65%;
    margin-left: 10px;
    margin-right: 5px;
    
`;


const Avatar = styled.Image`
    width: 100px;
    height: 100px;
    border-radius: 10px;
    justify-content: center;
    align-items: center;

`;

const InfoArea = styled.View`
    justify-content: space-evenly;
    width: 90%;
    padding:5px;
`;

const UserName = styled.Text`
    font-size: 15px;
    font-weight: bold;
    color: #FFF;

`;

const SeeProfileButton = styled.View`
    width: 60px;
    height: 30px;
    border: 1px solid #fff;
    border-radius: 10px;
    justify-content: center;
    align-items: center;
    background-color: #1E3148;
`;

const SeeProfileButtonText = styled.Text`
    font-size: 15px;
    color: #FFFFFF;
    font-weight: bold;
`;

export default ({data}) => {
    const navigation = useNavigation();

    const handleClick = () => {
        navigation.navigate('Barber', {
            id: data.id,
            avatar: data.avatar,
            name: data.name,
            stars: data.stars
        });
    }

    return (
        <Area onPress={handleClick}>
            <AreaAvatar onPress={handleClick}>
                <Avatar source={{uri: data.avatar}} />
                <Stars stars={data.stars} showNumber={true} />
            </AreaAvatar>

            <AreaInfo onPress={handleClick}>
                <InfoArea>
                    <UserName>{data.name}</UserName>

                    <SeeProfileButton>
                        <SeeProfileButtonText>Perfil</SeeProfileButtonText>
                    </SeeProfileButton>
                </InfoArea>
            </AreaInfo>
        </Area>
    );
}