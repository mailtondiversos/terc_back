import React, { useState, useContext} from 'react';
import styled from 'styled-components/native';

import { UserContext } from '../contexts/UserContext';

import HomeIcon from '../assets/home3.svg';
import SearchIcon from '../assets/search3.svg';
import TodayIcon from '../assets/today2.svg';
import FavoriteIcon from '../assets/favorite3.svg';
import AccountIcon from '../assets/account2.svg';


const TabArea = styled.View`
    height: 50px;
    background-color: #1E3148;
    flex-direction: row;
    border: 1px solid #1E3148;
    
`;

const TabItem = styled.TouchableOpacity`
    width: 50px;
    height: 40px;
    flex: 1;
    justify-content: center;
    align-items: center;
    margin-top: 5px;
    margin-bottom: 10px;
    margin-left: 5px;
    margin-right: 5px;
    margin-top: 6px;

`;
const TabItemCenter = styled.TouchableOpacity`
    width: 75px;
    height: 55px;
    justify-content: center;
    align-items: center;
    background-color: #FFF;
    border-radius: 2px;
    margin-top: -1px;
`;
const AvatarIcon = styled.Image`
    width: 26px;
    height: 26px;
    border-radius: 12px;
`;

const TextHome = styled.Text`
    width: 35px;
    height: 15px;
    justify-content: center;
    font-size: 11px;
    font-weight: bold;
    color: #FFF;

`;
const TextSearch = styled.Text`
width: 37px;
height: 15px;
justify-content: center;
font-size: 11px;
font-weight: bold;
color: #FFF;

`;

const TextFavorite = styled.Text`
width: 42px;
height: 15px;
justify-content: center;
font-size: 11px;
font-weight: bold;
color: #FFF;

`;

const TextAvatar = styled.Text`
width: 35px;
height: 15px;
justify-content: center;
font-size: 11px;
font-weight: bold;
color: #FFF;

`;

import Api from '../Api';


export default ({ state, navigation }) => {
    const { state:user } = useContext(UserContext);

    const [list, setList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [refreshing, setRefreshing] = useState(false);


    const getListFavorite = async () => {
        try{
            setLoading(true);
            let res = await Api.getListFavorite();
            console.log('ESSA È A LISTA DE FAVORITE >>>>>>>', res)
            if(res.list) {
                setList(res.list);
            } else {
                alert("Erro do else: "+res.error);
            }
            setLoading(false);

        }catch(e) {
            alert("Erro do catch: "+e);
        }
        setLoading(false);
        
    }

    const onRefresh = async () => {
        setRefreshing(false);
        setList([]);
        getListFavorite();
    }

    const goTo = async (screenName) => {
        setRefreshing(false);
        setList([]);
        getListFavorite();
        navigation.navigate(screenName);
    }

    return (
        <TabArea>
            <TabItem onPress={()=>goTo('Home')}>
                <HomeIcon style={{opacity: state.index===0? 1 : 0.5}} width="22" height="22" fill="#FFFFFF" />
                <TextHome style={{opacity: state.index===0? 1 : 0.5}}>Home</TextHome>
            </TabItem>
            <TabItem onPress={()=>goTo('Search')}>
                <SearchIcon style={{opacity: state.index===1? 1 : 0.5}} width="22" height="22" fill="#FFFFFF" />
                <TextSearch style={{opacity: state.index===1? 1 : 0.5}}>Search</TextSearch>
            </TabItem>
            <TabItemCenter onPress={()=>goTo('Appointments')}>
                <TodayIcon width="36" height="36" fill="#1E3148"  />
            </TabItemCenter>
            <TabItem onPress={()=>goTo('Favorites')}>
                <FavoriteIcon style={{opacity: state.index===3? 1 : 0.5}} width="22" height="22" fill="#FFFFFF" />
                <TextFavorite style={{opacity: state.index===3? 1 : 0.5 }}>Favorite</TextFavorite>
            </TabItem>
            <TabItem onPress={()=>goTo('Profile')}>
                {user.avatar != '' ?
                    <AvatarIcon source={{uri: user.avatar}} />
                    :
                    <AccountIcon style={{opacity: state.index===4? 1 : 0.5}} width="22" height="22" fill="#FFFFFF" />
                    
                }
                <TextAvatar style={{opacity: state.index===4? 1 : 0.5 }}>Perfil</TextAvatar>
            </TabItem>
        </TabArea>
    );
}