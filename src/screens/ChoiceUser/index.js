import React, { useState } from 'react';
import styled from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';
import Lottie from 'lottie-react-native';

import ChoiceUser from '../../assets/choiceUser.svg'; //retirar essa imagem
import ChoiceJobs from '../../assets/choiceJobs.svg'; //retirar essa imagem
import LoadPerfil from '../../animation/loadPerfil.json';
import UserPerfil from '../../animation/userPerfil.json';
import ProfessionPerfil from '../../animation/professionPerfil.json';






export default () => {

    const navigation = useNavigation();
    const giSignUpUser = () =>{
        navigation.navigate('SignUp');
    }

    return (
        <Container>
    
                <CustomTitle>Escolha seu perfil</CustomTitle>
                <InputArea>
                    <CustomButton  onPress={giSignUpUser}>
                        <CustomsTitleButtom>Em busca de profissionais ?</CustomsTitleButtom>
                        <Lottie  style={{width:150, height:150}} resizeMode="contain" source={UserPerfil} autoPlay loop />
                        <CustomButtonText>Perfil Cliente</CustomButtonText>
                    </CustomButton>
                    <Lottie  style={{width:65, height:65}} resizeMode="contain" source={LoadPerfil} autoPlay loop />                    
                    <CustomButton >
                        <CustomsTitleButtom>Em busca de clientes ?</CustomsTitleButtom>
                        <Lottie  style={{width:150, height:150}} resizeMode="contain" source={ProfessionPerfil} autoPlay loop />
                        <CustomButtonText>Perfil profisional</CustomButtonText>
                    </CustomButton>
                </InputArea>
        </Container>
    );
}

export const Container = styled.SafeAreaView`
    flex: 1;
    justify-content: center;
    align-items: center;
    background-color: #2A486D;
`;

export const InputArea = styled.View`
    width: 100%;
    padding: 20px;
    justify-content: center;
    align-items: center;
    margin-Top: -25%;
`;

const CustomButton = styled.TouchableOpacity`
    width: 80%;
    height: 35%;
    border-radius: 25px;
    justify-content: center;
    align-items: center;
    color:#FFFF;
    border: 1px solid #1E3148;
    background-color: rgba(0, 0, 0, 0.1);
    padding: 15px;
    paddingTop: 5px;
    paddingBottom: 5px;
    margin-bottom: 5%;
    margin-top: 5%;
`;
export const CustomButtonText = styled.Text`
    font-size: 15px;
    color: #FFFFFF;
    font-weight: bold;
    margin-top: 5%;
    justify-content: center;
    align-items: center;
`;
export const CustomsTitleButtom = styled.Text`
    font-size: 17px;
    color: #FFFFFF;
    margin-top: 5%;
    margin-Bottom: 2%;
    justify-content: center;
    align-items: center;
`;

export const CustomTitle = styled.Text`
    font-size: 17px;
    color: #FFFFFF;
    font-weight: bold;
    margin-top: 1%;
    margin-Bottom: 15%;
    margin-Top: 25%;
    justify-content: center;
    align-items: center;
`;