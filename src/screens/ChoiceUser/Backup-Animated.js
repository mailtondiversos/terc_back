/*import React, { useState } from 'react';
import { Animated } from 'react-native';
import styled from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';

import ChoiceUser from '../../assets/choiceUser.svg';
import ChoiceJobs from '../../assets/choiceJobs.svg';



export default () => {

    //const navigation = useNavigation();

    const [largura, setLargura] = useState(new Animated.Value(0));
    const [altura, setAltura] = useState(new Animated.Value(30));

    Animated.sequence([
        Animated.timing(
            largura,
            {
                toValue: 370,
                duration: 20000,
                useNativeDriver: true
            }
        ),
        Animated.timing(
            altura,
            {
                toValue: 40,
                duration: 10000,
                useNativeDriver: true
            }
        )
    ]).start();
    /*const { dispatch: userDispatch } = useContext(UserContext);
    const navigation = useNavigation();

    useEffect(()=>{
        const checkToken = async () => {
            const token = await AsyncStorage.getItem('token');

            if(token) {
                let res = await Api.checkToken(token);
                if(res.token) {

                    await AsyncStorage.setItem('token', res.token);

                    userDispatch({
                        type: 'setAvatar',
                        payload:{
                            avatar: res.data.avatar
                        }
                    });
                
                    navigation.reset({
                        routes:[{name:'MainUser'}]
                    });

                } else {
                    navigation.navigate('Instruction');
                }
            } else {
                navigation.navigate('Instruction');
            }
        }
        checkToken();
    }, []);
    const giSigInUser = () =>{
        navigation.navigate('SignIn');
    }

   

    return (
        <Container>
            <Animated.View Style={{
                width: largura, 
                height: altura, 
                backgroundColor: '#0000'
                }}>
                <CustomTitle>Escolha seu perfil</CustomTitle>

                <InputArea>
                    <CustomButton  onPress={giSigInUser}>
                        <CustomsTitleButtom>Cliente ?</CustomsTitleButtom>
                        <ChoiceUser width="100%" height="80"/>
                        <CustomButtonText>Acessar</CustomButtonText>
                    </CustomButton>
                    <CustomButton >
                        <CustomsTitleButtom>Profissional ?</CustomsTitleButtom>
                        <ChoiceJobs width="100%" height="80"/>
                        <CustomButtonText>Acessar</CustomButtonText>
                    </CustomButton>
                </InputArea>
            </Animated.View>
        </Container>
    );
}

export const Container = styled.SafeAreaView`
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const InputArea = styled.View`
    width: 100%;
    padding: 20px;
    justify-content: center;
    align-items: center;
    margin-Top: -25%;
`;

const CustomButton = styled.TouchableOpacity`
    width: 55%;
    height: 35%;
    border-radius: 25px;
    justify-content: center;
    align-items: center;
    color:#FFFF;
    border: 1px solid #1E3148;
    background-color: rgba(0, 0, 0, 0.5);
    padding: 20px;
    paddingTop: 5px;
    paddingBottom: 30px;
    margin-bottom: 5%;
    margin-top: 10%;
`;
export const CustomButtonText = styled.Text`
    font-size: 15px;
    color: #FFFFFF;
    font-weight: bold;
    margin-top: 5%;
    justify-content: center;
    align-items: center;
`;
export const CustomsTitleButtom = styled.Text`
    font-size: 15px;
    color: #FFFFFF;
    margin-top: 5%;
    margin-Bottom: 10%;
    justify-content: center;
    align-items: center;
`;

export const CustomTitle = styled.Text`
    font-size: 17px;
    color: #FFFFFF;
    font-weight: bold;
    margin-top: 5%;
    margin-Bottom: 10%;
    margin-Top: 25%;
    justify-content: center;
    align-items: center;
`;
*/