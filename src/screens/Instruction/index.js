import React, { useState, useEffect  } from 'react';
import { Platform, PermissionsAndroid} from 'react-native';
import { View, Text, Image, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Container } from './styles';
import AppIntroSlider from 'react-native-app-intro-slider';

import styled from 'styled-components/native';

const Slides = [
    {
        key:'1',
        title: 'Bem vindo ao Tools',
        text: 'Ferramenta que conecta o usuário cliente a profissionais recomendados!',
        image: require('../../assets/logoSlider1.png')
    },
    {
        key:'2',
        title: 'Encontre profissionais e empresas da sua região',
        text: 'Aqui você encontra o profissional ideal para o serviço!',
        image: require('../../assets/logoSlider2.png')
    },
    {
        key:'3',
        title: 'Desenvolvido no Amazonas por uma empresa 100% Manauara.',
        text: 'Construido com amor e carinho para você!',
        image: require('../../assets/logoSlider3.png')
    },

]

export default () => {

    const [showHome, setShowHome] = useState(false);
    const [coords, setCoords] = useState(null);
    const navigation = useNavigation();

    const goSigIn = () => {
        navigation.navigate('ChoiceUser');
    }

    useEffect(() => {
        const callLocation = async () => {
            setCoords(null);
            if(Platform.OS === 'Ios') {
                Geolocation.getCurrentPosition((info) => {
                    setCoords(info.coords);
                    console.log('IOS _INFO >>>>>>>>>>>> ', info);
                    console.log('IOS _INFO.COORDS  >>>>>>', info.coords);
                },
                    (error) => alert(error.message),
                    { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
                );
    
            } else {
            const requestLocationPermission = async () => {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    {
                        title: "Permissão de Acesso à Localização",
                        message: "Este aplicativo precisa acessar sua localização.",
                        buttonNeutral: "Pergunte-me depois",
                        buttonNegative: "Cancelar",
                        buttonPositive: "OK"
                    },
                );
                if (granted == PermissionsAndroid.RESULTS.GRANTED) {
                    console.log('granted >>>>>>>', granted)
                    setLoading(true);
                        Geolocation.getCurrentPosition((info) => {
                            setCoords(info.coords);
                            console.log('ANDROID_INFO >>>>>>>>>>>> ', info);
                            console.log('ANDROID_INFO.COORDS  >>>>>>', info.coords);
                        },
                            (error) => alert(error.message),
                            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
                        );
                } else {
                alert('Permissão de Localização negada');
                }
            };
            requestLocationPermission();
            }
            callLocation();
        }
    }, [])



    function renderSlides({item}){
        return(
            <View style={{flex:1}}>
                <Image
                    source={item.image}
                    style={{
                        resizeMode: 'cover',
                        height: '63%',
                        width: '100%'
                    }}
                />
                <Text
                    style={{
                        paddingTop: -25,
                        paddingBottom: 5,
                        fontSize: 23,
                        paddingHorizontal: 35,
                        fontWeight: 'bold',
                        color: '#2A486D',
                        alignSelf: 'center'
                    }}>
                    {item.title}
                </Text>

                <Text
                style={{
                    alignSelf: 'center',
                    color: '#b5b5b5',
                    paddingHorizontal: 35,
                    paddingBottom: 15,
                    fontSize: 18,
                    fontWeight: 'bold',
                }}>
                    {item.text}
                </Text>
            </View>
            

        )
    }

const TextNext = styled.Text`
    font-size: 15px;
    color: #3A5270;
    font-weight: bold;
    padding-Horizontal: 10px;
`;

const TextBack = styled.Text`
    font-size: 15px;
    color: #3A5270;
    font-weight: bold;
    padding-Horizontal: 10px;
`;

const TextDone = styled.Text`
    font-size: 15px;
    color: #3A5270;
    font-weight: bold;
    padding-Horizontal: 10px;
`;


    if(showHome){
        return (
            <Container>
                
            </Container>
        )
    }else{
        return (
            <AppIntroSlider
                renderItem={renderSlides}
                data={Slides}
                activeDotStyle={{
                    backgroundColor:'#2A486D',
                    width:30
                }}
                renderNextButton={() => <TextNext>Proximo >></TextNext>}
                renderDoneButton={() => <TextDone onPress={goSigIn}>Acessar</TextDone>}
            />
        );
    }
}