import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
    background-color: #2A486D;
    flex: 1;
    justify-content: center;
    align-items: center;
`;
export const InputArea = styled.View`
    width: 100%;
    padding: 30px;
    margin-top: -40px;
`;

export const CustomButton = styled.TouchableOpacity`
    height: 50px;
    background-color: rgba(0, 0, 0, 0.5);
    border-radius: 15px;
    justify-content: center;
    align-items: center;
`;
export const CustomButtonText = styled.Text`
    font-size: 17px;
    color: #FFFFFF;
    font-weight: bold;
`;

export const SignMessageButton = styled.TouchableOpacity`
    flex-direction: row;
    justify-content: center;
    margin-top: 50px;
    margin-bottom: 20px;
`;
export const SignMessageButtonText = styled.Text`
    font-size: 16px;
    color: #FFFFFF;
    justify-content: center;
    align-items: center;
    padding: 5px;
    justify-content: center;
    align-items: center;
`;
export const SignMessageButtonTextBold = styled.Text`
    font-size: 15px;
    background-color: rgba(0, 0, 0, 0.5);
    color: #FFFFFF;
    border-radius: 8px;
    font-weight: bold;
    margin-left: 5px;
    padding: 4px;
    justify-content: center;
    align-items: center;

`;

export const TitleSignUp1 = styled.Text`
    font-size: 17px;
    font-weight: bold;
    color: #FFFFFF;
    justify-content: center;
    align-items: center;
    margin-bottom: 35px;
    margin-top: 15px;
    
`;

export const TitleSignUp2 = styled.Text`
    font-size: 17px;
    font-weight: bold;
    color: #FFFFFF;
    justify-content: center;
    align-items: center;
    margin-bottom: 2px;
    margin-top: 5px;
    
`;

export const BackButton = styled.TouchableOpacity`
    flex-direction: row;
    justify-content: space-between;
    margin-top: 2px;
    margin-bottom: -100px;
    padding: 20px;
    margin-right: 300px;
`;

