import React, { useState, useContext } from 'react';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import Lottie from 'lottie-react-native';

import { UserContext } from '../../contexts/UserContext';

import {
    Container,
    InputArea,
    CustomButton,
    BackButton,
    CustomButtonText,
    TitleSignUp1,
    SignMessageButton,
    SignMessageButtonText,
    SignMessageButtonTextBold
} from './styles';

import Api from '../../Api';

import SignInput from '../../components/SignInput';

import BarberLogo from '../../assets/tools2.svg';
import EmailIcon from '../../assets/email.svg';
import LockIcon from '../../assets/lock.svg';
import Back from '../../animation/back.json';

export default () => {
    const { dispatch: userDispatch } = useContext(UserContext);
    const navigation = useNavigation();

    const [emailField, setEmailField] = useState('');
    const [passwordField, setPasswordField] = useState('');

    const handleSignClick = async () => {
        if(emailField != '' && passwordField != '') {

            let json = await Api.signIn(emailField, passwordField);

            if(json.token) {
                await AsyncStorage.setItem('token', json.token);

                userDispatch({
                    type: 'setAvatar',
                    payload:{
                        avatar: json.data.avatar
                    }
                });

                navigation.reset({
                    routes:[{name:'MainTab'}]
                });
            } else {
                alert('E-mail e/ou senha errados!');
            }

        } else {
            alert("Preencha os campos!");
        }
    }

    const backChoiceUser = () => {
        navigation.reset({
            routes: [{name: 'ChoiceUser'}]
        });
    }

    const handleMessageButtonClick = () => {
        navigation.reset({
            routes: [{name: 'SignUp'}]
        });
    }
    return (
        <Container>
            <BarberLogo width="100%" height="120" />

            <TitleSignUp1>Encontre profissionais recomendados</TitleSignUp1>
                       
            <InputArea>
                <SignInput
                    IconSvg={EmailIcon}
                    placeholder="E-mail"
                    value={emailField}
                    onChangeText={t=>setEmailField(t)}
                />

                <SignInput
                    IconSvg={LockIcon}
                    placeholder="Senha"
                    value={passwordField}
                    onChangeText={t=>setPasswordField(t)}
                    password={true}
                />

                <CustomButton onPress={handleSignClick}>
                    <CustomButtonText>ENTRAR</CustomButtonText>
                </CustomButton>
            </InputArea>


                <TitleSignUp1>_________________ ou  _________________</TitleSignUp1>

            <SignMessageButton onPress={handleMessageButtonClick}>
                <SignMessageButtonText>Ainda não possui uma conta?</SignMessageButtonText>
                <SignMessageButtonTextBold>Registre-se</SignMessageButtonTextBold>
            </SignMessageButton>

            <BackButton onPress={backChoiceUser}>
                <Lottie  style={{width:30, height:30}} resizeMode="contain" source={Back} autoPlay loop />
            </BackButton>

        </Container>
    );
}