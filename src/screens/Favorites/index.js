import React, { useState, useEffect }  from 'react';
import {RefreshControl} from 'react-native';


import { 
    Container,
    Scroller,
    HeaderArea,
    HeaderTitle,
    SearchButton,
    SubArea,
    SubTitle,

    LocationArea,
    LocationInput,
    LocationFinder,

    LoadingIcon,
    ListArea 
} from './styles';

import Api from '../../Api';
import CustomHeaderBar from '../../components/CustomHeaderBar';
import TerceirosListFavorite from '../../components/TerceirosListFavorite';



export default () => {

    const [list, setList] = useState([]);
    const [refreshing, setRefreshing] = useState(false);
    const [loading, setLoading] = useState(false);
    

    const getListFavorite = async () => {
        try{
            let res = await Api.getListFavorite();
            console.log('ESSA È A LISTA DE FAVORITE >>>>>>>', res)
            if(res.list) {
                setList(res.list);
            } else {
                alert("Erro do else: "+res.error);
            }
            setLoading(false);

        }catch(e) {
            alert("Erro do catch: "+e);
        }
        setLoading(false);
        
    }

    
    const onRefresh = async () => {
        setRefreshing(false);
        setList([]);
        getListFavorite();
    }

    useEffect(()=>{
        setLoading(true);
        setList([]);
        getListFavorite();
        
    }, []);



    return (
        <Container>
            <CustomHeaderBar />
            <Scroller refreshControl={
                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }>

                <SubArea>
                    <SubTitle>Sua lista defavoritos!</SubTitle>
                </SubArea>

                {loading &&
                    <LoadingIcon size="large" color="#FFFFFF" />
                }
                
                <ListArea>       
                    {list.map((item, k)=>(
                        <TerceirosListFavorite key={k} list={item} />
                    ))}
                </ListArea>
            </Scroller>
        </Container>
    );
}
