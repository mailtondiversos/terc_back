import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
    flex: 1;
    background-color: #133154;
`;

export const Scroller = styled.ScrollView`
    flex: 1;
    padding: 20px;
`;

export const HeaderArea = styled.View`
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;
export const HeaderTitle = styled.Text`
    width: 230px;
    font-size: 16px;
    font-weight: bold;
    color: #FFF;
`;

export const SubArea = styled.View`
    flex-direction: row;
    align-items: center;
    margin-top: 10px;
`;

export const SubTitle = styled.Text`
    width: 400px;
    font-size: 16px;
    font-weight: bold;
    color: #FFF;
`;

export const SearchButton = styled.TouchableOpacity`
    width: 26px;
    height: 26px;
`;

export const LocationArea = styled.View`
    background-color: rgba(0, 0, 0, 0.5);
    height: 45px;
    border-radius: 10px;
    flex-direction: row;
    align-items: center;
    padding-left: 15px;
    padding-right: 15px;
    margin-top: 15px;
`;
export const LocationInput = styled.TextInput`
    flex: 1;
    font-size: 16px;
    color: #FFFFFF;
`;
export const LocationFinder = styled.TouchableOpacity`
    width: 24px;
    height: 24px;
`;
export const LoadingIcon = styled.ActivityIndicator`
    margin-top: 50px;
`;
export const ListArea = styled.View`
    margin-top: 15px;
    margin-bottom: 15px;
`;