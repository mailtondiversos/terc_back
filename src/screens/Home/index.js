import React, { useState, useEffect } from 'react';
import { Platform, RefreshControl, PermissionsAndroid} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { request, PERMISSIONS } from 'react-native-permissions';
import Geolocation from '@react-native-community/geolocation';

import Api from '../../Api';

import {
    Container,
    Scroller,
    HeaderArea,
    HeaderTitle,
    SearchButton,
    SubArea,
    SubTitle,


    LocationArea,
    LocationInput,
    LocationFinder,

    LoadingIcon,
    ListArea
} from './styles';

import BarberItem from '../../components/BarberItem';
import CustomHeaderBar from '../../components/CustomHeaderBar';

import SearchIcon from '../../assets/search.svg';
import MyLocationIcon from '../../assets/my_location.svg';
import { set } from 'react-native-reanimated';


export default () => {
    const navigation = useNavigation();

    const [locationText, setLocationText] = useState('');
    const [coords, setCoords] = useState(null);
    const [loading, setLoading] = useState(false);
    const [list, setList] = useState([]);
    const [refreshing, setRefreshing] = useState(false);

    const callLocation = async () => {
        
        setCoords(null);
        
        if(Platform.OS === 'Ios') {
            Geolocation.getCurrentPosition((info) => {
                setCoords(info.coords);
                console.log('IOS _INFO >>>>>>>>>>>> ', info);
                console.log('IOS _INFO.COORDS  >>>>>>', info.coords);
                getBarbers();
            },
                (error) => alert(error.message),
                { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
            );

        } else {
        const requestLocationPermission = async () => {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: "Permissão de Acesso à Localização",
                    message: "Este aplicativo precisa acessar sua localização.",
                    buttonNeutral: "Pergunte-me depois",
                    buttonNegative: "Cancelar",
                    buttonPositive: "OK"
                },
            );
            if (granted == PermissionsAndroid.RESULTS.GRANTED) {
                console.log('granted >>>>>>>', granted)
                setLoading(true);
                setLocationText('');
                setList([]);
                    Geolocation.getCurrentPosition((info) => {
                        setCoords(info.coords);
                        console.log('ANDROID_INFO >>>>>>>>>>>> ', info);
                        console.log('ANDROID_INFO.COORDS  >>>>>>', info.coords);
                        getBarbers();
                    },
                        (error) => alert(error.message),
                        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
                    );
            } else {
            alert('Permissão de Localização negada');
            }
        };
        requestLocationPermission();
        }
    }


   const getBarbers = async () => {
        setLoading(true);
        setList([]);

        let lat = null;
        let lng = null;
        console.log('Essa é o valor do lat e lng  >>>', lat, lng)
 
        if(coords) {
            lat = coords.latitude;
            lng = coords.longitude;

            console.log('Essa é a merda do lat e lng DENTRO DO IF >>>', lat, lng)
        }

        let res = await Api.getBarbers(lat, lng, locationText);
        console.log('LISTA DE TERCEIRO DA API >>>>>', res.data);
        if(res.error == '') {
            if(res.loc){
                setLocationText(res.loc);
            }
            setList(res.data);
        } else {
            alert("Erro: "+res.error);
        }
        setLoading(false);
    }

    useEffect(()=>{
        setLoading(true);
        setList([]);
        getBarbers();
    }, []);

    const onRefresh = () => {
        setRefreshing(false);
        getBarbers();
    }

    const handleLocationSearch = () => {
        setCoords({});
        getBarbers();
    }

    return (
        <Container>
            <CustomHeaderBar />
            <Scroller refreshControl={
                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }>
                
                <HeaderArea>
                    <HeaderTitle numberOfLines={2}>Encontre o profissional ideal para o serviço!</HeaderTitle>
                    <SearchButton onPress={()=>navigation.navigate('Search')}>
                        <SearchIcon width="26" height="26" fill="#FFF" />
                    </SearchButton>
                </HeaderArea>

                <LocationArea>
                    <LocationInput
                        placeholder="Em qual cidade você está?"
                        placeholderTextColor="#FFFFFF"
                        value={locationText}
                        onChangeText={t=>setLocationText(t)}
                        onEndEditing={handleLocationSearch}
                    />
                    <LocationFinder onPress={callLocation}>
                        <MyLocationIcon width="24" height="24" fill="#FFFFFF" />
                    </LocationFinder>
                </LocationArea>

                <SubArea>
                    <SubTitle>Profissionais proximos de você!</SubTitle>
                </SubArea>

                {loading &&
                    <LoadingIcon size="large" color="#FFFFFF" />
                }
                
                <ListArea>
                    {list.map((item, k)=>(
                        <BarberItem key={k} data={item} />
                    ))}
                </ListArea>

            </Scroller>
        </Container>
    );
}