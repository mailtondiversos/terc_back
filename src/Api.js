import AsyncStorage from '@react-native-community/async-storage';
import { add } from 'react-native-reanimated';

const BASE_API = 'http://192.168.0.38:3000/api';


export default {
    checkToken: async (token) => {
        const req = await fetch(`${BASE_API}/auth/refresh`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({token})
        });
        const json = await req.json();
        console.log('retorno voltando a API ==>> ', json)
        return json;
    },
    signIn: async (email, password) => {
        const req = await fetch(`${BASE_API}/auth/login`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({email, password})
        });
        const json = await req.json();        
        return json;
    },
    signUp: async (name, email, password) => {
        const req = await fetch(`${BASE_API}/user`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({name, email, password})
        });
        const json = await req.json();        
        return json;
    },
    logout: async () => {
        const token = await AsyncStorage.getItem('token');

        const req = await fetch(`${BASE_API}/auth/logout`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({token})
        });
        const json = await req.json();        
        return json;
    },


    getBarbers: async (lat, lng, address=null) => {
        try{
            const token = await AsyncStorage.getItem('token');

            console.log("LAT >>>>", lat);
            console.log("LNG >>>>", lng);
            console.log("ADDRESS  >>>>", address);
            console.log("TOKEN  >>>>", token);

            //const req = await fetch(`${BASE_API}/terceiros?token=${token}`);
            const req = await fetch(`${BASE_API}/terceiros?token=${token}&lat=${lat}&lng=${lng}&address=${address}`);
            const json = await req.json();
            return json;

        }catch (error){
            console.log(error, 'Errors');
                        
        }
    },

    getBarber: async (id) => {
        const token = await AsyncStorage.getItem('token');
        const req = await fetch(`${BASE_API}/terceiros/${id}?token=${token}`);
        const json = await req.json();
        console.log(json);
        return json;
    },
    setFavorite: async (barberId) => {
        const token = await AsyncStorage.getItem('token');

        const req = await fetch(`${BASE_API}/user/favorite`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({token, terceiros:barberId})
        });
        const json = await req.json();        
        return json;
    },
    getListFavorite: async () => {
        const token = await AsyncStorage.getItem('token');

        const req = await fetch(`${BASE_API}/user/favorites?token=${token}`);
        const json = await req.json();        
        return json;
    },
    
    setAppointment: async (userId,
        service,
        selectedYear,
        selectedMonth,
        selectedDay,
        selectedHour) => {
        const token = await AsyncStorage.getItem('token');

        const req = await fetch(`${BASE_API}/user/appointment`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                token,
                id: userId,
                service,
                year: selectedYear,
                month: selectedMonth,
                day: selectedDay,
                hour: selectedHour
            })
        });
        const json = await req.json();        
        return json;
    }

};