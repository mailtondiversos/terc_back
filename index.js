/**
 * @format
 */

 import 'react-native-gesture-handler';
 import {AppRegistry} from 'react-native';
 import App from './App';
 import index from './src/screens/SignIn/index'
 import {name as appName} from './app.json';
 
 AppRegistry.registerComponent(appName, () => App);